var restify = require("restify");
require("./node_app/utilities");
require("./node_app/db");

var server = restify.createServer();

server.use(restify.acceptParser(server.acceptable));
server.use(restify.authorizationParser());
server.use(restify.dateParser());
server.use(restify.queryParser());
server.use(restify.jsonp());
server.use(restify.bodyParser());

server.get(/^\/api\/(.*)\/(.*)/, require("./node_app/getRequests"));

server.get(/.*/,
    restify.serveStatic({
        directory:"./public/",
        "default":"index.html"
    })
);

var port = process.env.PORT || 3000;
server.listen(port,function(){
	console.log("Server Started. Press Ctrl+c to quit server")
})